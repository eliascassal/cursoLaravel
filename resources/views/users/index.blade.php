@extends('welcome')

@section('content')
	<h1>Usuarios</h1>
	<table class="table">
		<thead>
			<th>ID</th>
			<th>Nombre</th>
			<th>Email</th>
			<th>Role</th>
			<th>Acciones</th>
		</thead>
		<tbody>
			@foreach ($users as $user)
				<tr>
					<td>{{ $user->id }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>
						{{ $user->roles->pluck('display_name')->implode(', ') }}
					</td>
					<td>
						<a 
						href="{{ route('usuarios.edit', $user->id) }}" 
						class="btn btn-info btn-xs">Editar</a>
						<form style="display: inline;" 
							action="{{ route('usuarios.destroy', $user->id) }}"  method="POST">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}

							<button type="submit" class="btn btn-danger btn-xs">Eliminar</button>
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection