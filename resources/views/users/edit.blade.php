@extends('welcome')

@section('content')
	<h1>Editar usuario</h1>
	<hr>
	@if (session()->has('info'))
		<div class="alert alert-success">
			{{ session('info') }}
		</div>
	@endif
	<form action="{{ route('usuarios.update', $user->id) }}" method="post">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<div class="form-group">
			<label for="name">Nombre</label>
			<input type="text" class="from-control" name="name" value="{{ $user->name }}">
			{{ $errors->first('name') }}
		</div>
		<div class="form-group">
			<label for="email">Email</label>
			<input type="email" class="from-control" name="email" value="{{ $user->email }}">
			{{ $errors->first('email') }}
		</div>
		<input type="submit" value="Enviar" class="btn btn-primary">
	</form>
@endsection