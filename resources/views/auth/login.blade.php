@extends('welcome')

@section('content')
	<h1>Login</h1>
	<form method="post" action="/login">
		{{ csrf_field() }}
		<input type="email" name="email" placeholder="Email" required class="from-control">
		<input type="password" name="password" placeholder="Password" required class="from-control">
		<input type="submit" value="Entrar" class="btn btn-primary">
	</form>
@endsection