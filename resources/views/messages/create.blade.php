@extends('welcome')

@section('content')
	<h1>Contacto</h1>
	<h4>Escríbeme un mensaje</h4>
	<br>
	@if (session('info'))
		<h1>{{ session('info') }}</h1>
	@endif
	<form action="{{ route('mensajes.store') }}" method="post">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="nombre">Nombre</label>
			<input type="text" class="from-control" name="nombre" value="{{ old('nombre') }}">
			{{ $errors->first('nombre') }}
		</div>
		<div class="form-group">
			<label for="email">Email</label>
			<input type="email" class="from-control" name="email" value="{{ old('email') }}">
			{{ $errors->first('email') }}
		</div>
		<div class="form-group">
			<label for="mensaje">Mensaje</label>
			<textarea name="mensaje" class="from-control">{{ old('mensaje') }}</textarea>
			{{ $errors->first('mensaje') }}
		</div>
		<input type="submit" value="Enviar" class="btn btn-primary">
	</form>
@endsection