@extends('welcome')

@section('content')
	<h1>Todos los mensajes</h1>
	<hr>
	<table class="table">
		<thead>
			<th>ID</th>
			<th>Nombre</th>
			<th>Email</th>
			<th>Mensaje</th>
			<th>Acciones</th>
		</thead>
		<tbody>
			@foreach ($messages as $message)
				<tr>
					<td>{{ $message->id }}</td>
					<td>
						<a href="{{ route('mensajes.show', $message->id) }}">
							{{ $message->nombre }}
						</a>
					</td>
					<td>{{ $message->email }}</td>
					<td>{{ $message->mensaje }}</td>
					<td>
						<a href="{{ route('mensajes.edit', $message->id) }}" class="btn btn-info btn-xs">Editar</a>
						<form style="display: inline;" action="{{ route('mensajes.destroy', $message->id) }}"  method="POST">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}

							<button type="submit" class="btn btn-danger btn-xs">Eliminar</button>
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection