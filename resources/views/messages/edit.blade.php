@extends('welcome')

@section('content')
	<h1>Editar mensaje de {{ $message->nombre }}</h1>
	<hr>
	<form action="{{ route('mensajes.update', $message->id) }}" method="post">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<div class="form-group">
			<label for="nombre">Nombre</label>
			<input type="text" class="from-control" name="nombre" value="{{ $message->nombre }}">
			{{ $errors->first('nombre') }}
		</div>
		<div class="form-group">
			<label for="email">Email</label>
			<input type="email" class="from-control" name="email" value="{{ $message->email }}">
			{{ $errors->first('email') }}
		</div>
		<div class="form-group">
			<label for="mensaje">Mensaje</label>
			<textarea name="mensaje" class="from-control">{{ $message->mensaje }}</textarea>
			{{ $errors->first('mensaje') }}
		</div>
		<input type="submit" value="Enviar" class="btn btn-primary">
	</form>
@endsection