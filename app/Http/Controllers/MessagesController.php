<?php

namespace App\Http\Controllers;

use DB;

use App\Message;

use Carbon\Carbon;

use Illuminate\Http\Request;

class MessagesController extends Controller
{

    function __construct()
    {
        $this->middleware('auth', ['except' => ['create', 'store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$messages = DB::table('messages')->get();

        $messages = Message::all();

        return view('messages.index', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validacion del Request
        $this->validate($request, [
            'nombre' => 'required',
            'email' => 'required|email',
            'mensaje' => 'min:4'
        ]);
        // Guardar mensaje
        // DB::table('messages')->insert([
        //     'nombre' => $request->nombre,
        //     'email' => $request->email,
        //     'mensaje' => $request->mensaje,
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now()
        // ]);

        Message::create([
            'nombre' => $request->nombre,
            'email' => $request->email,
            'mensaje' => $request->mensaje
        ]);
        // Redireccionar mensaje

        return redirect()->route('mensajes.create')->with('info', 'Hemos recibido tu mensaje');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = Message::findOrFail($id);

        return view('messages.show', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = Message::findOrFail($id);

        return view('messages.edit', compact('message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Actualizar
        // DB::table('messages')->where('id', $id)->update([
        //     'nombre' => $request->nombre,
        //     'email' => $request->email,
        //     'mensaje' => $request->mensaje,
        //     'updated_at' => Carbon::now()
        // ]);

        $message = Message::findOrFail($id);

        $message->update([
            'nombre' => $request->nombre,
            'email' => $request->email,
            'mensaje' => $request->mensaje
        ]);

        // Redireccionar
        return redirect()->route('mensajes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Eliminar mensaje
        $message = Message::findOrFail($id)->delete();
        
        // Redireccionar
        return redirect()->route('mensajes.index');
    }
}
